// CREATING A BASIC SERVER SETUP

/*
	NodeJS provides us with a "runtime environment (RTE)" that will execute our program/application.

	RTE (Runtime Environment)
	--> an environment/system in which a program can be executed.
*/

/*
	TASK: Create a standard server setup using 'plain' NodeJS.

	1. Identify and prepare the components/ingredients that you would need in order to execute the task.

		The main ingredient when creating a server using plain Node is: HTTP (HyperText Transfer Protocol).
		HTTP --> is a 'built-in' module of NodeJS that establishes a connection and makes transfer of data over HTTP possible.

		You need to be able to get the components first. Use the require() directive

		require() directive --> a function that gathers and acquires certain packages that are used to build our application.
*/

	const http = require('http');
	// http provides all the components needed to establish a server.

	//http contains the utility constructor called 'createServer()', which creates an HTTP server.
	// http.createServer()

/*
	2. Identify and describe a location where the connection will happen. In order to provide a proper medium for both parties (client and server), we will then bind the connection to the desired port number.

*/
	let port = 4000;

/*
	3. Inside the server constructor, insert a method in which will be used to describe the connection that was established. Identify the interaction between the client and the server and pass them down as arguments of the method.

	4. Bind/Assign the connection to the address. The listen() is used to bind and listen to a specific port whenever it's being accessed by the computer.
*/

	http.createServer((request, response) => {
	//write() --> inserts messages or input to our page
		response.write(`Server of Batch165 on Port ${port}`);
		response.write('Hello Batch 165');
	//end() --> identifies a point where the transmission of data will end.
		response.end();
	}).listen(port);

	console.log('Server is Running');

	//NOTE: When creating a project, we should always think of more efficient ways in executing even the smallest task.

/*
	TASK: Integrate the use of an NPM Project in NodeJS.

	1. Initialize an NPM (Node Package Manager) into the local project.

		2 Ways to perform this task:
			Method 1: npm init

			Method 2: npm init -y
				-y (yes)

			package.json --> 'the heart of any Node projects'. It records all the inportant metadata about the project, (libraries, packages, function) that makes up the system or application.

	Now that we have integrated an NPM into our project, let's solve some issues we are facing in our local project.

		Issue: Needing to restart the project whenever changes occur.

		Solve: Using NPM lets install a dependency that will automatically fix (hotfix) changes in our app.

		=> 'nodemon' package

	Basic Skills in Using NPM:
	a. Install package
		Method 1: npm install <name of package>
		Method 2: npm i <name of package>

		NOTE: You can also install multiple packages at the same time.
			nodemon, express, bcrypt

	b. Uninstall package
		Method 1: npm uninstall <name of package>
		Method 2: npm un <name of package>

			NOTE: You can also uninstall multiple packages at the same time.

	nodemon utility --> is a CLI utility tool. Its task is to wrap your node js app and watch for any changes in the file system and automatically restart/hotfix the process.

	If this is the first time, your local machine would use a platform/technology, the machine would need to recognize the technology first.

		Issue: command not foud after running nodemon server

		Solve: Install it on a 'global' scale.
		global --> the file system structure of the whole machine
			npm install -g nodemon

        //After installing the new package, register it to the package.json file.

        //nodemon -> auto hotfix for changes done in the project.
        //node -> plain run time environment
*/




	// press Ctrl + c to terminate the terminal/server